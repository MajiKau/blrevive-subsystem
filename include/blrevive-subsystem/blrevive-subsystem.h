#pragma once

#include <BLRevive/BLRevive.h>

// static reference to the BLRevive API 
static BLRE::BLRevive* BLReviveAPI;

// logger instance of this module
static BLRE::LogFactory::TSharedLogger Log;