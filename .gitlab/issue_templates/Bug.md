## Summary

> Summerize the bug encountered precisely.

## Expected behaviour

> Explain how things should behave normally.

## Current  behaviour

> Explain the bugged behaviour with all details you can provide.

## Steps to reproduce

> **Very Important**: list down all steps to reproduce the issue.

## (optional) Logs / Screenshots

> List all relevant log files or screenshots here.

## (optional) Possible fixes

> **Only for developers**: If you are able to, provide a fix in form of a code block or merge/pull request.
